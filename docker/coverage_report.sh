#/bin/bash
# Example call:
# ./coverage_report.sh REF BED BAM

REF=$1
BED=$2
BAM=$3

/opt/coverageAnalysis/run_coverage_analysis.sh -l -a -R ts_report.html -g -A $BED $REF $BAM
retVal=$?
if [ $retVal -ne 0 ]; then
    exit $retVal
fi

REPORT=report.html
cat << EOD > $REPORT
<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE HTML>
<html>
<head>
<base target="_parent"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title>Torrent Coverage Analysis Report</title>
<style>
  body {
    background: #F0F0F4;
    padding: 0;
    margin: 8px;
  }
</style>
</head>
<body>
<div class='center' style='width:100%;height:100%'>
<h1><center>Coverage Analysis Report</center></h1>
<div style="width:860px;margin-left:auto;margin-right:auto;">
<pre>
EOD

cat *stats.cov.txt >> $REPORT

echo "</pre>" >> $REPORT

for i in *.png
do
	echo "<img src='data:image/png;base64,$(base64 $i | tr -d '\n')'>" >> $REPORT
done

echo "</div></body></html>" >> $REPORT
